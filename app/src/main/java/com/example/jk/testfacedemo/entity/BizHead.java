package com.example.jk.testfacedemo.entity;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/3/21/021.
 */

public class BizHead implements Serializable {
    private String bizRetCode;
    private String bizRetMsg;
    private static final long serialVersionUID = 1L;

    public String getBizRetCode() {
        return bizRetCode;
    }

    public void setBizRetCode(String bizRetCode) {
        this.bizRetCode = bizRetCode;
    }

    public String getBizRetMsg() {
        return bizRetMsg;
    }

    public void setBizRetMsg(String bizRetMsg) {
        this.bizRetMsg = bizRetMsg;
    }
}
