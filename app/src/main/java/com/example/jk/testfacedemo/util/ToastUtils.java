package com.example.jk.testfacedemo.util;

import android.support.annotation.NonNull;
import android.widget.Toast;

import com.example.jk.testfacedemo.App;

/**
 * ToastUtils 利用单例模式，解决重命名toast重复弹出的问题
 */
public class ToastUtils {
    private static Toast mToast;

    public static void s(@NonNull String mString) {
        if (mToast == null) {
            mToast = Toast.makeText(App.getInstance(), mString, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(mString);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    public static void s(int messageID) {
        if (mToast == null) {
            mToast = Toast.makeText(App.getInstance(), messageID, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(messageID);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    public static void l(@NonNull String mString) {
        if (mToast == null) {
            mToast = Toast.makeText(App.getInstance(), mString, Toast.LENGTH_LONG);
        } else {
            mToast.setText(mString);
            mToast.setDuration(Toast.LENGTH_LONG);
        }
        mToast.show();
    }

    public static void l(int messageID) {
        if (mToast == null) {
            mToast = Toast.makeText(App.getInstance(), messageID, Toast.LENGTH_LONG);
        } else {
            mToast.setText(messageID);
            mToast.setDuration(Toast.LENGTH_LONG);
        }
        mToast.show();
    }

}
