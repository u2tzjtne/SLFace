package com.example.jk.testfacedemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.arcsoft.facetracking.AFT_FSDKEngine;
import com.arcsoft.facetracking.AFT_FSDKError;
import com.arcsoft.facetracking.AFT_FSDKFace;
import com.arcsoft.facetracking.AFT_FSDKVersion;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.example.jk.testfacedemo.R;
import com.example.jk.testfacedemo.constant.Const;
import com.example.jk.testfacedemo.constant.Url;
import com.example.jk.testfacedemo.entity.DetectionResultEntity;
import com.example.jk.testfacedemo.entity.UploadBean;
import com.example.jk.testfacedemo.util.LogUtils;
import com.example.jk.testfacedemo.util.OkHttpUtils;
import com.example.jk.testfacedemo.util.ToastUtils;
import com.google.gson.Gson;
import com.guo.android_extend.widget.CameraFrameData;
import com.guo.android_extend.widget.CameraGLSurfaceView;
import com.guo.android_extend.widget.CameraSurfaceView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DetectionActivity extends Activity implements CameraSurfaceView.OnCameraListener, Camera.AutoFocusCallback {

    @BindView(R.id.surfaceView)
    CameraSurfaceView mSurfaceView;
    @BindView(R.id.gl_surfaceView)
    CameraGLSurfaceView mGLSurfaceView;
    @BindView(R.id.detect_layout)
    RelativeLayout mDetectLayout;
    @BindView(R.id.qr_layout)
    LinearLayout mQRLayout;
    @BindView(R.id.camera_layout)
    FrameLayout mCameraLayout;
    @BindView(R.id.image_qr)
    ImageView imageQr;

    private Camera mCamera;
    private List<AFT_FSDKFace> result = new ArrayList<>();
    private AFT_FSDKVersion version = new AFT_FSDKVersion();
    private AFT_FSDKEngine engine = new AFT_FSDKEngine();
    private AFT_FSDKFace mAFT_FSDKFace = null;

    private String door_id = null;

    private boolean isWorking = false;

    //private boolean isOpenDoor = false;

    private boolean mIsPortrait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置全屏
        View decorView6 = getWindow().getDecorView();
        int uiOptions6 = View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView6.setSystemUiVisibility(uiOptions6);
        setContentView(R.layout.activity_detection);
        //判断屏幕方向
        mIsPortrait = this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        LogUtils.d("屏幕方向:" + mIsPortrait);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra("qr_url");
            door_id = intent.getStringExtra("id");
            if (!TextUtils.isEmpty(door_id) && !TextUtils.isEmpty(url)) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.mipmap.icon)
                        .error(R.mipmap.icon)
                        .priority(Priority.HIGH);
                Glide.with(DetectionActivity.this)
                        .load(url)
                        .apply(options)
                        .into(imageQr);
                mSurfaceView.setOnCameraListener(this);
                mSurfaceView.setupGLSurafceView(mGLSurfaceView, true, true, 270);
                //初始化人脸检测引擎（FT）
                AFT_FSDKError err = engine.AFT_FSDK_InitialFaceEngine(Const.APP_ID, Const.FT_KEY,
                        AFT_FSDKEngine.AFT_OPF_0_HIGHER_EXT, 16, 5);
                LogUtils.d("AFT_FSDK_InitialFaceEngine =" + err.getCode());
                err = engine.AFT_FSDK_GetVersion(version);
                LogUtils.d("AFT_FSDK_GetVersion:" + version.toString() + "," + err.getCode());
            } else {
                ToastUtils.s("获取门店信息失败!");
            }

        } else {
            ToastUtils.s("获取门店信息失败!");
        }

    }

    @Override
    public Camera setupCamera() {
        int _width = mSurfaceView.getWidth();
        int _height = mSurfaceView.getHeight();
        LogUtils.d("SurfaceView宽高:" + _width + "x" + _height);
        //前摄像头
        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPreviewFormat(ImageFormat.NV21);
            ///Camera.Size size = getOptimalSize(parameters.getSupportedPreviewSizes(), _height, _width);
            //Camera.Size size = getCloselyPreSize(_width, _height, parameters.getSupportedPreviewSizes());
            //LogUtils.d("预览宽高:" + size.width + "x" + size.height);
            parameters.setPreviewSize(1440, 1080);
            mCamera.setParameters(parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCamera;
    }


    /**
     * Calculate the optimal size of camera preview
     *
     * @param sizes
     * @param w
     * @param h
     * @return
     */
    private Camera.Size getOptimalSize(List<Camera.Size> sizes, int w, int h) {

        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) w / h;
        LogUtils.d("屏幕比例:" + targetRatio);
        if (sizes == null)
            return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            LogUtils.d(size.width + "x" + size.height + "-----比例:" + ratio);
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - h) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - h);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - h) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - h);
                }
            }
        }
        return optimalSize;
    }


    /**
     * 通过对比得到与宽高比最接近的尺寸（如果有相同尺寸，优先选择）
     *
     * @param surfaceWidth  需要被进行对比的原宽
     * @param surfaceHeight 需要被进行对比的原高
     * @param preSizeList   需要对比的预览尺寸列表
     * @return 得到与原宽高比例最接近的尺寸
     */
    protected Camera.Size getCloselyPreSize(int surfaceWidth, int surfaceHeight,
                                            List<Camera.Size> preSizeList) {
        int ReqTmpWidth;
        int ReqTmpHeight;
        // 当屏幕为垂直的时候需要把宽高值进行调换，保证宽大于高
        if (mIsPortrait) {
            ReqTmpWidth = surfaceHeight;
            ReqTmpHeight = surfaceWidth;
        } else {
            ReqTmpWidth = surfaceWidth;
            ReqTmpHeight = surfaceHeight;
        }
        //先查找preview中是否存在与surfaceview相同宽高的尺寸
        for (Camera.Size size : preSizeList) {
            if ((size.width == ReqTmpWidth) && (size.height == ReqTmpHeight)) {
                return size;
            }
        }
        // 得到与传入的宽高比最接近的size
        float reqRatio = ((float) ReqTmpWidth) / ReqTmpHeight;
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        Camera.Size retSize = null;
        for (Camera.Size size : preSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }
        return retSize;
    }

    @Override
    public void setupChanged(int format, int width, int height) {

    }

    @Override
    public boolean startPreviewImmediately() {
        return true;
    }

    @Override
    public Object onPreview(byte[] data, int width, int height, int format, long timestamp) {
        AFT_FSDKError err = engine.AFT_FSDK_FaceFeatureDetect(data, width, height, AFT_FSDKEngine.CP_PAF_NV21, result);
        LogUtils.d("AFT_FSDK_FaceFeatureDetect =" + err.getCode());
        LogUtils.d("Face=" + result.size());
        for (AFT_FSDKFace face : result) {
            LogUtils.d("Face:" + face.toString());
        }
        Rect[] rects = new Rect[result.size()];
        for (int i = 0; i < result.size(); i++) {
            rects[i] = new Rect(result.get(i).getRect());
        }
        if (!result.isEmpty()) {
            mAFT_FSDKFace = result.get(0).clone();
        } else {
            mAFT_FSDKFace = null;
        }
        if (mAFT_FSDKFace != null) {
            /*
             * 1.保存
             * 2.暂停扫描
             * 3.上传
             * 4.显示结果
             * 5.重新启动
             * */
            if (!isWorking) {
                isWorking = true;
                File pictureFile = new File(Environment.getExternalStorageDirectory(), "face_image.jpg");
                if (!pictureFile.exists()) {
                    try {
                        pictureFile.createNewFile();
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        YuvImage image = new YuvImage(data, ImageFormat.NV21, width, height, null);   //将NV21 data保存成YuvImage
                        //图像压缩
                        image.compressToJpeg(mAFT_FSDKFace.getRect(), 100, fos);   // 将NV21格式图片，以质量70压缩成Jpeg，并得到JPEG数据流
                    } catch (IOException e) {
                        e.printStackTrace();
                        isWorking = false;
                    }
                } else {
                    try {
                        pictureFile.delete();
                        pictureFile.createNewFile();
                        FileOutputStream fos = new FileOutputStream(pictureFile);
                        YuvImage image = new YuvImage(data, ImageFormat.NV21, width, height, null);   //将NV21 data保存成YuvImage
                        //图像压缩
                        image.compressToJpeg(mAFT_FSDKFace.getRect(), 100, fos);   // 将NV21格式图片，以质量70压缩成Jpeg，并得到JPEG数据流
                    } catch (IOException e) {
                        e.printStackTrace();
                        isWorking = false;
                    }
                }
                if (isWorking) {
                    uploadFile(pictureFile.getAbsolutePath());
                }
            }
        }
        result.clear();
        return rects;
    }

//    //显示上传布局
//    private void showUploadLayout() {
//        mCameraLayout.setVisibility(View.GONE);
//        mUploadLayout.setVisibility(View.VISIBLE);
//    }
//
//    //显示相机布局
//    private void showCameraLayout() {
//        mCameraLayout.setVisibility(View.VISIBLE);
//        mUploadLayout.setVisibility(View.GONE);
//    }

    /**
     * 上传图片
     */
    public void uploadFile(String filePath) {
        String url = Url.UPLOAD_PIC;
        LogUtils.d("开始上传---路径：" + filePath);
        File file = new File(filePath);
        RequestBody fileBody = RequestBody.create(MediaType.parse("*/*"), file);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(), fileBody)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Call call = new OkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.d("文件上传返回数据异常；" + e);
                //TODO 上传失败没做处理
                isWorking = false;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                LogUtils.d("文件上传返回数据；" + result);
                if (!"".equals(result) && result.contains("bizHead")) {
                    UploadBean uploadBean = new Gson().fromJson(result, UploadBean.class);
                    if ("1000".equals(uploadBean.getBizHead().getBizRetCode())) {
                        String mUrl = uploadBean.getBizInfo().getModels().get(0).getUrl();
                        String name = uploadBean.getBizInfo().getModels().get(0).getOriginName();
                        if (!TextUtils.isEmpty(mUrl)) {
                            LogUtils.d("文件上传成功");
                            if (isWorking) {
                                getDetectionResult(mUrl);
                            }
                        } else {
                            LogUtils.d("文件上传失败");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    isWorking = false;
                                }
                            });
                        }
                    } else {
                        LogUtils.d("文件上传失败");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                isWorking = false;
                            }
                        });
                    }
                }
            }
        });
    }

    //获取比对结果
    private void getDetectionResult(String picUrl) {
        OkHttpUtils.doGet(Url.GET_RESULT + picUrl, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.d("获取比对结果返回数据异常；" + e);
                //TODO 获取比对结果失败没做处理
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isWorking = false;
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                LogUtils.d("获取比对结果返回数据: " + result);
                try {
                    JSONObject object = new JSONObject(result);
                    int code = object.getInt("code");
                    if (code == 200) {
                        final DetectionResultEntity entity = new Gson().fromJson(result, DetectionResultEntity.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtils.s("欢迎" + entity.getBody().getNickName() + "光临!");
                                openDoor();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtils.s("识别失败");
                                openDoor();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void test() {
        ToastUtils.s("开门成功");
        isWorking = false;
    }

    //开门
    private void openDoor() {
        OkHttpUtils.doGet(Url.OPEN_DOOR + door_id, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.s("开门失败");
                        isWorking = false;
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String result = response.body().string();
                LogUtils.d("开门返回的数据: " + result);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.s("开门成功");
                        isWorking = false;
//                        //isOpenDoor = true;
//                        //showUploadLayout();
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                //showCameraLayout();
//                                isOpenDoor = false;
//                            }
//                        }, 2000);
                    }
                });
            }
        });
    }

    @Override
    public void onBeforeRender(CameraFrameData data) {

    }

    @Override
    public void onAfterRender(CameraFrameData data) {
        mGLSurfaceView.getGLES2Render().draw_rect((Rect[]) data.getParams(), Color.GREEN, 2);
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        if (success) {
            LogUtils.d("对焦完成");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AFT_FSDKError err = engine.AFT_FSDK_UninitialFaceEngine();
        LogUtils.d("AFT_FSDK_UninitialFaceEngine =" + err.getCode());
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }
}
