package com.example.jk.testfacedemo.entity;

public class DetectionResultEntity {

    /**
     * code : 200
     * msg : ok
     * body : {"status":"","wxOpenid":"o3mE-1lklHquwTFUbyanqPs06FSc","userRoles":"2","recId":"F00B003F77937A50E73D7B00BC03F3E0","groupId":"8340F14E36A1329FBB41D458E1D5182F","nickName":"付化峰","pic":"","regChanel":"","chatroomFlag":"","mobile":"18652991279","idCard":"","regTime":"","regWay":"","pusher":"","userLevel":"","locLng":"","locLat":"","aliOpenid":"","createTime":""}
     */

    private int code;
    private String msg;
    private BodyBean body;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BodyBean getBody() {
        return body;
    }

    public void setBody(BodyBean body) {
        this.body = body;
    }

    public static class BodyBean {
        /**
         * status :
         * wxOpenid : o3mE-1lklHquwTFUbyanqPs06FSc
         * userRoles : 2
         * recId : F00B003F77937A50E73D7B00BC03F3E0
         * groupId : 8340F14E36A1329FBB41D458E1D5182F
         * nickName : 付化峰
         * pic :
         * regChanel :
         * chatroomFlag :
         * mobile : 18652991279
         * idCard :
         * regTime :
         * regWay :
         * pusher :
         * userLevel :
         * locLng :
         * locLat :
         * aliOpenid :
         * createTime :
         */

        private String status;
        private String wxOpenid;
        private String userRoles;
        private String recId;
        private String groupId;
        private String nickName;
        private String pic;
        private String regChanel;
        private String chatroomFlag;
        private String mobile;
        private String idCard;
        private String regTime;
        private String regWay;
        private String pusher;
        private String userLevel;
        private String locLng;
        private String locLat;
        private String aliOpenid;
        private String createTime;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getWxOpenid() {
            return wxOpenid;
        }

        public void setWxOpenid(String wxOpenid) {
            this.wxOpenid = wxOpenid;
        }

        public String getUserRoles() {
            return userRoles;
        }

        public void setUserRoles(String userRoles) {
            this.userRoles = userRoles;
        }

        public String getRecId() {
            return recId;
        }

        public void setRecId(String recId) {
            this.recId = recId;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getRegChanel() {
            return regChanel;
        }

        public void setRegChanel(String regChanel) {
            this.regChanel = regChanel;
        }

        public String getChatroomFlag() {
            return chatroomFlag;
        }

        public void setChatroomFlag(String chatroomFlag) {
            this.chatroomFlag = chatroomFlag;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getIdCard() {
            return idCard;
        }

        public void setIdCard(String idCard) {
            this.idCard = idCard;
        }

        public String getRegTime() {
            return regTime;
        }

        public void setRegTime(String regTime) {
            this.regTime = regTime;
        }

        public String getRegWay() {
            return regWay;
        }

        public void setRegWay(String regWay) {
            this.regWay = regWay;
        }

        public String getPusher() {
            return pusher;
        }

        public void setPusher(String pusher) {
            this.pusher = pusher;
        }

        public String getUserLevel() {
            return userLevel;
        }

        public void setUserLevel(String userLevel) {
            this.userLevel = userLevel;
        }

        public String getLocLng() {
            return locLng;
        }

        public void setLocLng(String locLng) {
            this.locLng = locLng;
        }

        public String getLocLat() {
            return locLat;
        }

        public void setLocLat(String locLat) {
            this.locLat = locLat;
        }

        public String getAliOpenid() {
            return aliOpenid;
        }

        public void setAliOpenid(String aliOpenid) {
            this.aliOpenid = aliOpenid;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
