package com.example.jk.testfacedemo.entity;

import java.util.List;

public class QrEntity {

    /**
     * code : 200
     * msg : ok
     * body : {"bizHead":{"bizRetCode":"1000","bizRetMsg":""},"bizInfo":{"models":[{"originName":"1532518560520_openDoor:fzm01.png","url":"http://img.senlan24h.com/SHILU/1/1532518560577-8qaxkq2uOiwrz34PBt1N.png","size":93195,"num":-1,"firstImg":"","completeFlag":0}]},"transInfo":""}
     */

    private int code;
    private String msg;
    private BodyBean body;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BodyBean getBody() {
        return body;
    }

    public void setBody(BodyBean body) {
        this.body = body;
    }

    public static class BodyBean {
        /**
         * bizHead : {"bizRetCode":"1000","bizRetMsg":""}
         * bizInfo : {"models":[{"originName":"1532518560520_openDoor:fzm01.png","url":"http://img.senlan24h.com/SHILU/1/1532518560577-8qaxkq2uOiwrz34PBt1N.png","size":93195,"num":-1,"firstImg":"","completeFlag":0}]}
         * transInfo :
         */

        private BizHeadBean bizHead;
        private BizInfoBean bizInfo;
        private String transInfo;

        public BizHeadBean getBizHead() {
            return bizHead;
        }

        public void setBizHead(BizHeadBean bizHead) {
            this.bizHead = bizHead;
        }

        public BizInfoBean getBizInfo() {
            return bizInfo;
        }

        public void setBizInfo(BizInfoBean bizInfo) {
            this.bizInfo = bizInfo;
        }

        public String getTransInfo() {
            return transInfo;
        }

        public void setTransInfo(String transInfo) {
            this.transInfo = transInfo;
        }

        public static class BizHeadBean {
            /**
             * bizRetCode : 1000
             * bizRetMsg :
             */

            private String bizRetCode;
            private String bizRetMsg;

            public String getBizRetCode() {
                return bizRetCode;
            }

            public void setBizRetCode(String bizRetCode) {
                this.bizRetCode = bizRetCode;
            }

            public String getBizRetMsg() {
                return bizRetMsg;
            }

            public void setBizRetMsg(String bizRetMsg) {
                this.bizRetMsg = bizRetMsg;
            }
        }

        public static class BizInfoBean {
            private List<ModelsBean> models;

            public List<ModelsBean> getModels() {
                return models;
            }

            public void setModels(List<ModelsBean> models) {
                this.models = models;
            }

            public static class ModelsBean {
                /**
                 * originName : 1532518560520_openDoor:fzm01.png
                 * url : http://img.senlan24h.com/SHILU/1/1532518560577-8qaxkq2uOiwrz34PBt1N.png
                 * size : 93195.0
                 * num : -1.0
                 * firstImg :
                 * completeFlag : 0.0
                 */

                private String originName;
                private String url;
                private double size;
                private double num;
                private String firstImg;
                private double completeFlag;

                public String getOriginName() {
                    return originName;
                }

                public void setOriginName(String originName) {
                    this.originName = originName;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public double getSize() {
                    return size;
                }

                public void setSize(double size) {
                    this.size = size;
                }

                public double getNum() {
                    return num;
                }

                public void setNum(double num) {
                    this.num = num;
                }

                public String getFirstImg() {
                    return firstImg;
                }

                public void setFirstImg(String firstImg) {
                    this.firstImg = firstImg;
                }

                public double getCompleteFlag() {
                    return completeFlag;
                }

                public void setCompleteFlag(double completeFlag) {
                    this.completeFlag = completeFlag;
                }
            }
        }
    }
}
