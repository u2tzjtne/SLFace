package com.example.jk.testfacedemo.util;

import android.app.ProgressDialog;
import android.content.Context;

public class PGDialogUtils {
    /**
     * @author yj
     * 简易的加载过渡工具
     * 动画显示过程中不允许用户手动取消
     * 只能通过代码取消
     * 经过测试,context不允许设置为application的context,
     */

    private static ProgressDialog mDialog;

    private PGDialogUtils() {
    }

    /**
     * 显示
     *
     * @param title
     * @param msg
     */
    public static void show(Context context, String title, String msg) {
        if (mDialog != null) {
            mDialog = null;
        }
        mDialog = new ProgressDialog(context);
        // 点击屏幕不隐藏
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(true);
        mDialog.setTitle(title);
        mDialog.setMessage(msg);
        if (!mDialog.isShowing()) {
            mDialog.show();
        }
    }


    /**
     * 显示
     *
     * @param context
     * @param msg
     */
    public static void show(Context context, String msg) {
        show(context, "", msg);
    }

    /**
     * 显示
     *
     * @param context
     */
    public static void show(Context context) {
        show(context, "", "请稍后...");
    }

    /**
     * 隐藏
     */
    public static void dismiss() {
        if (mDialog == null) {
            return;
        }

        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
