package com.example.jk.testfacedemo.entity;

import java.util.List;

// FIXME generate failure  field _$TransInfo124
// FIXME generate failure  field _$BizInfo7

/**
 * Created by Administrator on 2017/3/27/027.
 */

public class UploadBean extends BaseBean {

    private BizInfoBean bizInfo;
    private String transInfo;

    public BizInfoBean getBizInfo() {
        return bizInfo;
    }

    public void setBizInfo(BizInfoBean bizInfo) {
        this.bizInfo = bizInfo;
    }

    public String getTransInfo() {
        return transInfo;
    }

    public void setTransInfo(String transInfo) {
        this.transInfo = transInfo;
    }

    public static class BizInfoBean {
        private List<ModelsBean> models;

        public List<ModelsBean> getModels() {
            return models;
        }

        public void setModels(List<ModelsBean> models) {
            this.models = models;
        }

        public static class ModelsBean {
            /**
             * originName : IMG_20170327_135938.jpg
             * url : http://img.synapse.com//1/1/1490597244075.jpg
             */

            private String originName;
            private String url;

            public String getOriginName() {
                return originName;
            }

            public void setOriginName(String originName) {
                this.originName = originName;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }
}
