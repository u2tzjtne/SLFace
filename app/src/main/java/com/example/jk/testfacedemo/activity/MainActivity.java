package com.example.jk.testfacedemo.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.example.jk.testfacedemo.R;
import com.example.jk.testfacedemo.constant.Url;
import com.example.jk.testfacedemo.entity.QrEntity;
import com.example.jk.testfacedemo.util.LogUtils;
import com.example.jk.testfacedemo.util.OkHttpUtils;
import com.example.jk.testfacedemo.util.PGDialogUtils;
import com.example.jk.testfacedemo.util.ToastUtils;
import com.google.gson.Gson;
import com.pgyersdk.update.PgyUpdateManager;
import com.pgyersdk.update.UpdateManagerListener;
import com.pgyersdk.update.javabean.AppBean;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements UpdateManagerListener {

    @BindView(R.id.face_detection)
    Button faceDetection;
    @BindView(R.id.edit_text)
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        PgyUpdateManager.register(this);
    }

    @OnClick(R.id.face_detection)
    public void onViewClicked() {
        String id = editText.getText().toString().trim();
        if (!TextUtils.isEmpty(id)) {
            PGDialogUtils.show(MainActivity.this);
            getQr(id);
        } else {
            ToastUtils.s("请输入门店ID");
        }
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void startActivity(String url,String id) {
        Intent intent = new Intent(MainActivity.this, DetectionActivity.class);
        intent.putExtra("qr_url", url);
        intent.putExtra("id",id);
        startActivity(intent);
    }

    //获取门店二维码
    private void getQr(final String id) {
        Map<String, String> params = new HashMap<>();
        params.put("page", "pages/index/index");
        params.put("scene", "openDoor:" + id);
        params.put("width", "430");
        String json = new Gson().toJson(params);
        OkHttpUtils.doPostForJSON(Url.GET_QR, json, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                LogUtils.d("获取二维码失败" + e.getMessage());
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PGDialogUtils.dismiss();
                        ToastUtils.s("获取二维码失败,请重试!");
                    }
                });
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                PGDialogUtils.dismiss();
                String result = response.body().string();
                if (!TextUtils.isEmpty(result)) {
                    LogUtils.d("获取二维码返回的数据: " + result);
                    QrEntity entity = new Gson().fromJson(result, QrEntity.class);
                    if (entity != null) {
                        if (entity.getCode() == 200) {
                            String url = entity.getBody().getBizInfo().getModels().get(0).getUrl();
                            if (!TextUtils.isEmpty(url)) {
                                MainActivityPermissionsDispatcher.startActivityWithPermissionCheck(MainActivity.this, url,id);
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ToastUtils.s("获取二维码失败,请重试!");
                                    }
                                });
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ToastUtils.s("获取二维码失败,请重试!");
                                }
                            });
                        }
                    }
                }

            }
        });
    }

    @OnPermissionDenied({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void OnPermissionDenied() {

    }

    @Override
    public void onNoUpdateAvailable() {

    }

    @Override
    public void onUpdateAvailable(AppBean appBean) {
        String updateNote;
        if (TextUtils.isEmpty(appBean.getReleaseNote())) {
            updateNote = "发现新版本,快去下载吧!";
        } else {
            updateNote = appBean.getReleaseNote();
        }
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("更新")
                .setMessage(updateNote)
                .setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setNegativeButton(
                        "确定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri uri = Uri.parse(Url.URL_APP_DOWNLOAD_URL);
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        })
                .show();
    }

    @Override
    public void checkUpdateFailed(Exception e) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(MainActivity.this, requestCode, grantResults);
    }
}
