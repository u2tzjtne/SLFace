package com.example.jk.testfacedemo.constant;

public class Url {
    public static final String UPLOAD_PIC = "http://upload.jssns.cn/upload/SHILU";
    public static final String GET_RESULT = "http://www.senlan24h.com/market/v1/show/userFace/search?facePic=";
    public static final String OPEN_DOOR = "http://www.senlan24h.com/market/v1/qcCode?state=";
    public static final String GET_QR = "https://www.senlan24h.com/market/v1/miniQrcode/create";
    public static final String URL_APP_DOWNLOAD_URL = "https://www.pgyer.com/h5hi";
}
