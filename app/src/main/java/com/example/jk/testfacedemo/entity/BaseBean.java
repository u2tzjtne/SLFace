package com.example.jk.testfacedemo.entity;


import java.io.Serializable;


public class BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private BizHead bizHead;

    public BizHead getBizHead() {
        return bizHead;
    }

    public void setBizHead(BizHead bizHead) {
        this.bizHead = bizHead;
    }
}
